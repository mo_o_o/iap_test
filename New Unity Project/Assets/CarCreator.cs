﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarCreator : MonoBehaviour
{
    public GameObject car;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CreateCar()
    {
        Instantiate(car, new Vector3(0f,0f), Quaternion.Euler(new Vector3(0, 150, 0)));
    }
}
