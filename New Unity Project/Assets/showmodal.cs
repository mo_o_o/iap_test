﻿using Michsky.UI.ModernUIPack;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class showmodal : MonoBehaviour
{

    public ModalWindowManager myModalWindow; // Your mw variable
    

    public void YourFunction()
    {
        
        ////myModalWindow.titleText = "New Title"; // Change title
        //myModalWindow.descriptionText = "New Description"; // Change desc
        myModalWindow.UpdateUI(); // Update UI
        myModalWindow.OpenWindow(); // Open window
        myModalWindow.CloseWindow(); // Close window
        myModalWindow.AnimateWindow(); // Close/Open window automatically
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
